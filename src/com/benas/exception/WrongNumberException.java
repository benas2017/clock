package com.benas.exception;

public class WrongNumberException extends Exception{
    public WrongNumberException(String message) {
        super(message);
    }
}
