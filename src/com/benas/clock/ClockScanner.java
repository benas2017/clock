package com.benas.clock;

import com.benas.exception.WrongNumberException;

import java.util.InputMismatchException;
import java.util.Scanner;

public class ClockScanner {

    private int minutes;
    private int hours;

    public ClockScanner() {
        selectHoursAndMinutes();
    }

    public void selectHoursAndMinutes() {

        Scanner scanner = new Scanner(System.in);
        boolean isItRunning = true;

        while (isItRunning) {
            try {
                System.out.println("Please select hour (from 1 to 12)");
                this.hours = scanner.nextInt();
                if (this.hours < 1 || this.hours > 12) {
                    throw new WrongNumberException("Wrong hour selected\n");
                }
                System.out.println("Please select minutes (from 0 to 59)");
                this.minutes = scanner.nextInt();
                if (this.minutes < 0 || this.minutes > 59) {
                    throw new WrongNumberException("Wrong minutes selected\n");
                }

                if (this.hours >= 1 && this.hours <= 12) {
                    isItRunning = false;
                }
            } catch (InputMismatchException e) {
                System.out.println("Wrong input.\n");
                scanner.next();
            } catch (WrongNumberException e) {
                System.out.println(e.getMessage());
            }
        }
    }

    public int getMinutes() {
        return minutes;
    }

    public int getHours() {
        return hours;
    }
}
