package com.benas.clock;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        boolean keepTheProgramRunning = true;
        Scanner scanner = new Scanner(System.in);

        while (keepTheProgramRunning) {

            Clock newClock = new Clock();
            newClock.countAngle();
            System.out.println("The angle between clock hands is: " + newClock.getAngle());

            System.out.println("\nPlease select next step: \n0 - Exit \nAny other symbol - try one more time");
            String choice = scanner.nextLine();
            if (choice.equals("0")) {
                keepTheProgramRunning = false;
            }
        }
    }
}
