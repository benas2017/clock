package com.benas.clock;

public class Clock {
    private int hours;
    private int minutes;
    private double angle;

    public Clock() {
        ClockScanner clockScanner = new ClockScanner();
        this.hours = clockScanner.getHours();
        this.minutes = clockScanner.getMinutes();
    }

    public void countAngle() {
        double hoursAngle = (this.hours*30) + (0.5*this.minutes);
        double minutesAngle = this.minutes*6;

        double difference = Math.abs(hoursAngle - minutesAngle);

        if (difference > 180) {
            difference = 360 - difference;
            setAngle(difference);
        } else {
            setAngle(difference);
        }
    }

    public double getAngle() {
        return angle;
    }

    private void setAngle(double angle) {
        this.angle = angle;
    }
}
